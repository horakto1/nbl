#!/usr/bin/python

import sys
import random
import csv
import lib
import matplotlib.pyplot as plt
from scipy.stats import norm	# Gauss distribution for probabilities


def genBases(minLen, maxLen):
	l = random.randint(minLen, maxLen)

	seq = []
	for i in range(0,l):
		x = random.randint(0, 3)

		if (x == 0):
			seq.append('A')
		elif (x == 1):
			seq.append('C')
		elif (x == 2):
			seq.append('G')
		elif (x == 3):
			seq.append('T')

	return seq

def genCurrent(bases, settings, table):
	seq = []
	i = settings['size']
	moves = [0]
	while (True):
		if (i >= len(bases)):
			break
		st = bases[i - settings['size']:i]
		intSt = lib.sToi(st)
		val = table[intSt][0]
		diff = random.gauss(0, table[intSt][1])
		seq.append(val + diff)

		move = random.random() - settings['stay_prob']
		moves.append(0)
		if (move > 0):
			i+=1
			moves[-1] += 1
			move -= settings['step_prob']
			if (move > 0):
				i+=1
				moves[-1] += 1

	return seq, moves

if (len(sys.argv) < 10 or len(sys.argv) > 10):
	print('Wrong usage! Use script.py minLen maxLen count size stay_prob skip_prob table.csv outBases.fasta outCurrents.fasta')
	exit(0);

minLen = int(sys.argv[1])
maxLen = int(sys.argv[2])
settings = {}
n = int(sys.argv[3])

settings['size'] = int(sys.argv[4])
settings['stay_prob'] = float(sys.argv[5])
settings['skip_prob'] = float(sys.argv[6])
settings['step_prob'] = 1 - settings['stay_prob'] - settings['skip_prob']

bFile = sys.argv[8]
cFile = sys.argv[9]

with open(sys.argv[7], 'r') as f:
	table = lib.readTable(f, settings['size'])

realAvg = 0
with open(bFile, 'w') as fb, open(cFile, 'w') as fc:
	for i in range(0, n):
		bases = genBases(minLen, maxLen)
		current, moves = genCurrent(bases, settings, table)
		realAvg += (len(current) / len(bases))

		basesStr = ''
		for b in bases:
			basesStr += b

		currStr = ''
		for c in current:
			currStr += (str(c) + ',')

		fb.write('>n' + str(i) + '_size' + str(settings['size']) + '_len' + str(len(bases)) + '\n')
		fb.write(basesStr + '\n')

		fc.write('>n' + str(i) + '_size' + str(settings['size']) + '_len' + str(len(bases)) + '\n')
		fc.write(currStr[:-1] + '\n')
