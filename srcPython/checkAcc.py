#!/usr/bin/python

import sys
from math import sqrt
from decimal import Decimal

def main(start):
	start = start.split('.fast')[0]
	loc = start + '_AlignLocal.fasta'
	glob = start + '_AlignGlobal.fasta'
	edit = start + '_AlignEdit.fasta'
	out = start + '_Results.txt'

	gAvg = 0
	lAvg = 0
	eAvg = 0

	gMin = 1
	lMin = 1
	eMin = 1

	gMax = 0
	lMax = 0
	eMax = 0

	gCs = []
	lCs = []
	eCs = []

	n = 0
	with open(out, 'w') as o:
		o.write(start + '\n')
		try:
			with open(loc, 'r') as l:
				while(True):
					labelL = l.readline()[:-1].split('_')
					l.readline()
					l.readline()

					if (labelL == ['']):
						break
					if (labelL[-1] == 'NA'):
						continue
					n += 1
					
					lC = float(labelL[-1])
					lCs.append(lC)

					lAvg += lC

					if (lC < lMin):
						lMin = lC
					if (lC > lMax):
						lMax = lC
				
				lAvg = lAvg / n
				lSdS = 0
				for i in range(len(lCs)):
					lSdS += (lCs[i] - lAvg) * (lCs[i] - lAvg)
				lSd = sqrt(lSdS / n)
				o.write('Local  Avg:\t' + str(lAvg) + '\tMin: ' + str(lMin) + '\tMax: ' + str(lMax) + '\tSd: ' + str(lSd) + '\n')
		except Exception:
			pass
		
		n = 0
		try:
			with open(glob, 'r') as g:
				while(True):
					label = g.readline()[:-1].split('_')
					g.readline()
					g.readline()

					if (label == ['']):
						break
					if (label[-1] == 'NA'):
						continue


					n += 1
					C = float(label[-1])
					gCs.append(C)

					gAvg += C

					if (C < gMin):
						gMin = C
					if (C > gMax):
						gMax = C
				
				gAvg = gAvg / n
				gSdS = 0
				for i in range(len(gCs)):
					gSdS += (gCs[i] - gAvg) * (gCs[i] - gAvg)
				gSd = sqrt(gSdS / n)
				o.write('Global Avg:\t' + str(gAvg) + '\tMin: ' + str(gMin) + '\tMax: ' + str(gMax) + '\tSd: ' + str(gSd)+ '\n')
		except Exception:
			pass

		n = 0
		try:
			with open(edit, 'r') as e:
				while(True):
					label = e.readline()[:-1].split('_')
					e.readline()
					e.readline()

					if (label == ['']):
						break
					if (label[-1] == 'NA'):
						continue

					n += 1
					C = float(label[-1])
					eCs.append(C)
					eAvg += C

					if (C < eMin):
						eMin = C
					if (C > eMax):
						eMax = C
				
				eAvg = eAvg / n
				eSdS = 0
				for i in range(len(eCs)):
					eSdS += (eCs[i] - eAvg) * (eCs[i] - eAvg)

				eSd = sqrt(eSdS / n)
				o.write('Edit   Avg:\t' + str(eAvg) + '\tMin: ' + str(eMin) + '\tMax: ' + str(eMax) + '\tSd: ' + str(eSd)+ '\n\n')
		except Exception:
			pass

		o.write('\n')

if __name__ == "__main__":
	if (len(sys.argv) < 2 or len(sys.argv) > 2):
		print('Wrong usage! Use script.py fileName')
		exit(0);
	
	main(sys.argv[1])




