#!/usr/bin/python

# Forward-Backward Algorithm

import sys						# managing argv
from scipy.stats import norm	# Gauss distribution for probabilities
from lib import *				# functions used in both viterbi and fwd-bkw

# Key for sorting by current
def getSecond(x):
	return x[1]

#----------------------------------------------------------------------------------------------------#
# Forward-Backward
def fwdBkw(currents, transM, emMeans, settings):
	l = settings['size']
	moveProbs = [settings['stay_prob'], settings['step_prob'], settings['skip_prob']]
	
	# forward part of the algorithm
	
	# First current
	fwd = [[]]
	for st in range(len(emMeans)):
		fwd[0].append(normVal(currents[0], emMeans[st][0], emMeans[st][1]))
		
	# Others
	for t in range(1, len(currents)):
		fwd.append([])
		maxCur = float('-inf')
		for st in range(len(emMeans)):
			prevFSum = 0
			for prevSt in range(len(emMeans)):
				prevFSum += fwd[t-1][prevSt] * getTransP(transM, moveProbs, prevSt, st, l)
			app = log(normVal(currents[t], emMeans[st][0], emMeans[st][1]) * prevFSum)
			if (app > maxCur):
				maxCur = app
			fwd[t].append(app)

		for i in range(len(fwd[t])):
			fwd[t][i] = exp(fwd[t][i] - maxCur)

	# backward part of the algorithm
	# Last current
	bkw = [[]]
	for st in range(len(emMeans)):
		bkw[0].append(normVal(currents[-1], emMeans[st][0], emMeans[st][1]))
		
	# Others
	for t in range(len(currents) - 2, -1, -1):
		bkw.append([])
		maxCur = float('-inf')
		vals = []
		for nextSt in range(len(emMeans)):
			vals.append(normVal(currents[t], emMeans[nextSt][0], emMeans[nextSt][1]))
			
		for st in range(len(emMeans)):
			nextSum = 0
			for nextSt in range(len(emMeans)):
				nextSum += bkw[-2][nextSt] * getTransP(transM, moveProbs, st, nextSt, l) * vals[nextSt]
			app = log(nextSum)
			if (app > maxCur):
				maxCur = app
			bkw[-1].append(app)

		for i in range(len(bkw[0])):
			bkw[-1][i] = exp(bkw[-1][i] - maxCur)

	# For each current save the most probable state
	prevSt = 0
	maxProb = float('-inf')
	for j in range(len(fwd[0])):
		post = fwd[0][j] * bkw[-1][j]
		if (post > maxProb):
			maxProb = post
			prevSt = j
	res = [prevSt]

	for i in range(1, len(fwd)):
		sts = []
		for j in range(len(fwd[i])):
			sts.append([j, fwd[i][j] * bkw[- i - 1][j]])
		sts = sorted(sts, key = getSecond, reverse = True)
		st = sts[0][0]
		
		if (prevSt != st and not follows(prevSt, st, 1, l) and not follows(prevSt, st, 2, l)):
			for s in sts[1:]:
				if (s[1] >= settings['smoothing'] * sts[0][1]):
					found = False
					for k in range(l-1):
						if (follows(prevSt, s[0], k, l)):
							st = s[0]
							found = True
							break
					if (found):
						break
				else:
					break
		res.append(st)
		prevSt = st
		
	return res

#----------------------------------------------------------------------------------------------------#
# Main
def main(cFile, tFile, outFile, sets):
	settings = {}
	settings['size'] = int(sets[0])
	settings['stay_prob'] = float(sets[1])
	settings['skip_prob'] = float(sets[2])
	settings['step_prob'] = 1 - (settings['stay_prob'] + settings['skip_prob'])
	settings['smoothing'] = float(sets[3])
	settings['consolidate'] = sets[4]

	with open(tFile, 'r') as f:
		emMeans = readTable(f, settings['size'])

	transM = createTM(settings['size'])

	with open(cFile, 'r') as f, open(outFile, 'w') as out:
		while(True):
			label, cur = readCurrents(f)
			if (label == ''):
				break
			out.write(">" + label + "\n")
			res = fwdBkw(cur, transM, emMeans, settings)
			if (settings['consolidate'] == 'y'):
				res2, moves = consolidateFB(res, settings['size'])
			else:
				res2, moves = consolidate(res, settings['size'])

			r = ""
			for x in res2:
				r += x
			out.write(r + "\n")
			if (outFile[-1] != 'a'):
				out.write("-" + label + "\n")
				movesS = "";
				for m in moves[:-1]:
					movesS += (str(m) + ",");
				movesS += (str(moves[-1]) + "\n");
				out.write(movesS);


if (len(sys.argv) < 9 or len(sys.argv) > 9):
	print('Wrong usage! Use script.py currents.fasta table.fasta output.fastt/a size stay_prob skip_prob smoothing consolidate')
	exit(0);

if __name__ == "__main__":
	main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4:])
