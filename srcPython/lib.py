import csv
import numpy as np
import h5py

#----------------------------------------------------------------------------------------------------#
# Conversions

def bToi(base):
	if (base == 'A'):
		return 0
	elif (base == 'C'):
		return 1
	elif (base == 'G'):
		return 2
	elif (base == 'T'):
		return 3

def iTob(i):
	if (i == 0):
		return 'A'
	elif (i == 1):
		return 'C'
	elif (i == 2):
		return 'G'
	elif (i == 3):
		return 'T'

def sToi(s):
	if (len(s) == 1):
		return bToi(s[0])
	return bToi(s[-1]) + (4 * sToi(s[:-1]))

def iTos(i, l = 3):
	if (l == 0):
		return []
	return iTos(int(i / 4), l - 1) + [iTob(i % 4)]

#----------------------------------------------------------------------------------------------------#
# Reading files

# Reads mean currents for each base l-tuple from csv file
def readTable(f, l = 3):
	table = []
	for i in range(4 ** l):
		table.append([0, 0])

	reader = csv.reader(f, delimiter=',', quotechar='"')
	for r in reader:
		table[sToi(r[0:l])][0] = float(r[-2])
		table[sToi(r[0:l])][1] = float(r[-1])
	return table

# Reads actual currents reported by nanopore for one record
def readCurrents(f):
	label = f.readline()
	if (not label or label == ''):
		return '', []
	return label[1:-1], [float(c) for c in f.readline().split(',')]

#----------------------------------------------------------------------------------------------------#
# Handling results

def consolidate(seq, l = 3):
	prev = iTos(seq[0], l)

	res = []
	for i in range(l):
		res.append(prev[i])

	moves = []
	for t in seq:
		s = iTos(t, l)

		if (prev == s):
			moves.append(0)
		else:
			for i in range(1, len(s) + 1):
				if (prev[i:] == s[:-i]):
					moves.append(i)
					for j in range(-i, 0, 1):
						res.append(s[j])
					break
		
		for i in range(len(s)):
			prev[i] = s[i]
	return res, moves


def consolidateFB(seq, l = 3):
	res = []
	moves = [0]
	prev = iTos(seq[0], l)
	for i in range(l):
		res.append(prev[i])
	sNext = ""
	m1 = 0
	m2 = 0

	for t in range(1, len(seq) - 1):
		s = iTos(seq[t], l)
		sNext = iTos(seq[t+1], l)

		if (seq[t-1] == seq[t]):
			moves.append(0)
		else:
			m1 = followsN(seq[t-1], seq[t], l)
			if (m1 >= 3 and followsN(seq[t], seq[t+1], l) >= 3):
				moves.append(-1)
				m2 = followsN(seq[t-1], seq[t+1], l);
				moves.append(m2)
				
				for j in range(len(s) - m2, len(s)):
					res.append(sNext[j])
				t += 1
			else:
				moves.append(m1)
				for j in range(len(s) - m1, len(s)):
					res.append(s[j])

	s = iTos(seq[-1], l)

	m1 = followsN(seq[-2], seq[-1], l)
	moves.append(m1)
	for j in range(len(s) - m1, len(s)):
		res.append(s[j])
		
	return res, moves

#----------------------------------------------------------------------------------------------------#
# Transitions

def getTransP(matrices, settings, prev, cur, l):
	p = 0
	for i in range(3):
		p += matrices[i][prev][cur] * settings[i]
	return p

def getTransPLog(matrices, settings, prev, cur, l):
	return log(getTransP(matrices, settings, prev, cur, l))

def follows(prev, new, move, l):
	d = 1
	for i in range(move):
		d *= 4
	m = 1
	for i in range(l - move):
		m *= 4
	return ((prev % m) == (new / d))

def followsN(prev, st, l):
	if (prev == st):
		return 0

	for i in range(l):
		if (follows(prev, st, i, l)):
			return i
	return l

def createTM(l = 3):
	p = 0.25
	matrices = []

	matrix = [ [0.0] * (4 ** l) for _ in range(4 ** l) ]
	for i in range(4 ** l):
		matrix[i][i] = 1.0
	matrices.append(matrix)

	matrix = [ [0.0] * (4 ** l) for _ in range(4 ** l) ]
	for prev in range(4 ** l):
		for new in range(4 ** l):
			if (follows(prev, new, 1, l)):
				matrix[prev][new] = p
	matrices.append(matrix)
	p /= 4

	matrix = [ [0.0] * (4 ** l) for _ in range(4 ** l) ]
	for prev in range(4 ** l):
		for new in range(4 ** l):
			if (follows(prev, new, 2, l)):
				matrix[prev][new] = p
	matrices.append(matrix)

	return matrices

#----------------------------------------------------------------------------------------------------#
# Math

def exp(x):
	if (x == float('-inf')):
		return 0
	return np.exp(x)

def log(x):
	if (x <= 0):
		return float('-inf')
	return np.log(x)

def normVal(x, m, s):
	inv_sqrt_2pi = 0.3989422804014327
	a = (x - m) / s
	return (inv_sqrt_2pi / s) * exp(-0.5 * a * a)

