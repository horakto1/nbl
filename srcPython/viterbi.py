#!/usr/bin/python

# Viterbi Algorithm

import sys						# managing argv
from scipy.stats import norm	# Gauss distribution for probabilities
from lib import *				# functions used in both viterbi and fwd-bkw

#----------------------------------------------------------------------------------------------------#
# Viterbi
def viterbi(currents, transM, emMeans, settings):
	l = settings['size']
	moveProbs = [settings['stay_prob'], settings['step_prob'], settings['skip_prob']]
	
	probs = [[]]
	prev = [[]]

	for st in range(len(emMeans)):
		probs[0].append(log(normVal(currents[0], emMeans[st][0], emMeans[st][1])))
		prev[0].append(None)

	for t in range(1, len(currents)):
		probs.append([])
		prev.append([])
			
		maxForCur = float('-inf')
		for st in range(len(emMeans)):
			pos = 0
			maxProb = probs[t-1][0] + getTransPLog(transM, moveProbs, 0, st, l) 
			for prevSt in range(1, len(emMeans)):
				tmp = probs[t-1][prevSt] + getTransPLog(transM, moveProbs, prevSt, st, l) 
				if (tmp > maxProb):
					maxProb = tmp
					pos = prevSt

			maxProb += log(normVal(currents[t], emMeans[st][0], emMeans[st][1]))
			if (maxProb >= maxForCur):
				maxForCur = maxProb

			probs[t].append(maxProb)
			prev[t].append(pos)

		for i in range(len(probs[t])):
			probs[t][i] -= maxForCur

	# The highest probability
	previous = 0
	mmax = probs[-1][0]
	for i in range(1, len(probs[-1][1:])):
		if (probs[-1][i] > mmax):
			mmax = probs[-1][i]
			previous = i
	opt = [previous]
	
	# Get most probable state and its backtrack
	# Follow the backtrack untill the first observation
	for t in range(len(probs) - 2, -1, -1):
		opt.insert(0, prev[t+1][previous])
		previous = prev[t+1][previous]

	return opt, maxProb

#----------------------------------------------------------------------------------------------------#
# Main
def main(cFile, tFile, outFile, sets):
	settings = {}
	settings['size'] = int(sets[0])
	settings['stay_prob'] = float(sets[1])
	settings['skip_prob'] = float(sets[2])
	settings['step_prob'] = 1 - (settings['stay_prob'] + settings['skip_prob'])

	with open(tFile, 'r') as f:
		emMeans = readTable(f, settings['size'])

	transM = createTM(settings['size'])
	with open(cFile, 'r') as f, open(outFile, 'w') as out:
		while(True):
			label, cur = readCurrents(f)
			if (label == ''):
				break
			print(label)
			out.write(">" + label + "\n")
			res, p = viterbi(cur, transM, emMeans, settings)
			res2, moves = consolidate(res, settings['size'])

			r = ""
			for x in res2:
				r += x
			out.write(r + "\n")

			if (outFile[-1] != 'a'):
				out.write("-" + label + "\n")
				movesS = "";
				for m in moves[:-1]:
					movesS += (str(m) + ",");
				movesS += (str(moves[-1]) + "\n");
				out.write(movesS);

if (len(sys.argv) < 7 or len(sys.argv) > 7):
	print('Wrong usage! Use script.py currents.fasta table.fasta output.fasta size stay_prob skip_prob')
	exit(0);

if __name__ == "__main__":
   	main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4:])
