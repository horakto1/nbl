#include "fwd_bkw.h"

using namespace std;

bool reverseCompare(pair<int, double> i, pair<int, double> j) { 
	return (i.second > j.second); 
}

// Forward-Backward
string fwdBkw(vector<double> currents, string predictions, vector<int> moves, vector<Table *> transM, vector<pair<double, double> > emMeans, unsigned int size, \
							double stay_prob, double step_prob, double skip_prob, vector< vector<unsigned int> > pred, vector< vector<unsigned int> > foll) {
	// Forward part of the algorithm
	string tmpStr;
	// First current
	vector<vector<double> > fwd(currents.size(), vector<double>(emMeans.size()));
	for (unsigned int st = 0; st < emMeans.size(); ++st) {
		fwd[0][st] = normVal(currents[0], emMeans[st].first, emMeans[st].second);
	}
	// Others
	double maxCur, sum, app;
	for (unsigned int t = 1; t < currents.size(); ++t) {
		maxCur = -DBL_MAX;
		for (unsigned int st = 0; st < emMeans.size(); ++st) {
			sum = 0;
			for (vector<unsigned int>::iterator prevSt = pred[st].begin(); prevSt != pred[st].end(); ++prevSt) {				
				sum += fwd[t-1][*prevSt] * getTransP(&transM, *prevSt, st, stay_prob, step_prob, skip_prob);
			}
			app = log(normVal(currents[t], emMeans[st].first, emMeans[st].second) * sum);
			if (app > maxCur) maxCur = app;
			fwd[t][st] = app;
		}
	
		for (unsigned int st = 0; st < fwd[t].size(); ++st) {
			fwd[t][st] = exp(fwd[t][st] - maxCur);
		}
	}

	//Backward part of the algorithm
	//Last current
	vector<vector<double> > bkw(currents.size(), vector<double>(emMeans.size()));
	for (unsigned int st = 0; st < emMeans.size(); ++st) {
		bkw[currents.size() - 1][st] = normVal(currents[currents.size() - 1], emMeans[st].first, emMeans[st].second);
	}
		
	//Others
	vector<double> vals(emMeans.size());
	for (int t = currents.size() - 2; t >= 0; --t) {
		maxCur = -DBL_MAX;
		for (unsigned int nextSt = 0; nextSt < vals.size(); ++nextSt) {
			vals[nextSt] = normVal(currents[t], emMeans[nextSt].first, emMeans[nextSt].second);
		}
			
		for (unsigned int st = 0; st < emMeans.size(); ++st) {
			sum = 0;
			for (vector<unsigned int>::iterator nextSt = foll[st].begin(); nextSt != foll[st].end(); ++nextSt) {				
				sum += bkw[t + 1][*nextSt] * getTransP(&transM, st, *nextSt, stay_prob, step_prob, skip_prob) * vals[*nextSt];
			}
			app = log(sum);
			if (app > maxCur) maxCur = app;
			bkw[t][st] = app;
		}

		for (unsigned int st = 0; st < bkw[t].size(); ++st) {
			bkw[t][st] = exp(bkw[t][st] - maxCur);
		}
	}

	// Quality assignment phase
	string res = "";
	vector<double> correct(size, 0);
	vector<double> all(size, 0);
	vector<double> sumP(predictions.length(), 0);
	vector<int> num(predictions.length(), 0);
	
	string state = "";
	unsigned int start = 0;
	double p;
	vector<pair<unsigned int, double> > sts;
	string tmp;
	for (unsigned int t = 0; t < fwd.size(); ++t) {
		if (moves[t] == -1) continue;
		start += moves[t];
		sts.clear();
		for (unsigned int j = 0; j < fwd[t].size(); ++j) {
			p = fwd[t][j] * bkw[t][j];
			state = iTos(j, size);

			for (unsigned int k = 0; k < size; ++k) {
				all[k] += p;
				if (state[k] == predictions[start + k]) {
					correct[k] += p;
				}
			}
		}
		for (unsigned int k = 0; k < size; ++k) {
			sumP[start + k] += (correct[k] / all[k]);
			all[k] = 0;
			correct[k] = 0;
			num[start + k] += 1;
		}
	}

	int Q;
	for (unsigned int i = 0; i < predictions.size(); ++i) {
		Q = round(-10 * log10(1 - (sumP[i] / num[i])));
		if (Q >= 41) Q = 40;
		res += ((char)Q + '!');
	}
	return res;
}

// Main
int main(int argc, char* argv[]) {
	if (argc < 8 || argc > 8) {
		cout << "Wrong usage! Use " << argv[0] << " currents.fasta predictions.fastt table.csv output.fastq size stay_prob skip_prob" << endl;
		return 1;
	}

	string cFile(argv[1]);
	string pFile(argv[2]);
	string tFile(argv[3]);
	string oFile(argv[4]);

	unsigned int size = stoi(string(argv[5]));
	double stay_prob = stof(string(argv[6]));
	double skip_prob = stof(string(argv[7]));
	double step_prob = 1 - stay_prob - skip_prob;
	
	vector<pair<double, double> > emMeans = readTable(tFile, size);
	vector<vector <unsigned int> > pred = findPredecessors(size);
	vector<vector <unsigned int> > foll = findFollowers(size);
	vector<Table *> transM = createTM(size);

	ifstream inCurr;
	inCurr.open(cFile.c_str());
	
	ifstream inPred;
	inPred.open(pFile.c_str());
	
	ofstream out;
	out.open(oFile.c_str());
	
	string label, label2, quality;
	char sep = ',';
	int mv = 0;
	double c = 0;
	vector<double> currents;
	vector<unsigned int> res;
	vector<int> moves;
	string predictions;
	inCurr >> sep;
	sep = ',';
	cout << "FwdBkwQuality" << endl;
	while (inCurr.good()) {
		currents.clear();
		inCurr >> label;
		
		while(inCurr >> c >> sep && sep == ',') {
			currents.push_back(c);
		}
		currents.push_back(c);

		inPred >> label2;
		inPred >> predictions;
		inPred >> label2;
		
		moves.clear();
		while(inPred >> mv >> sep && sep == ',') {
			moves.push_back(mv);
		}
		moves.push_back(mv);

		quality = fwdBkw(currents, predictions, moves, transM, emMeans, size, stay_prob, step_prob, skip_prob, pred, foll);
		
		out << "@" << label << endl;
		out << predictions << endl;
		out << "+" << label << endl;
		out << quality << endl;
	}

	for (unsigned int i = 0; i < transM.size(); ++i) {
		delete transM[i];
	}
	inCurr.close();
	out.close();
	return 0;
}
