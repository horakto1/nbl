#ifndef FORWARDBACKWARDASKLDJLASD
#define FORWARDBACKWARDASKLDJLASD

#include "lib.h"

using namespace std;

// Used for sorting
bool reverseCompare(pair<int, double> i, pair<int, double> j);

// Forward-Backward
vector<unsigned int> fwdBkw(vector<double> currents, vector<Table *> transM, vector<pair<double, double> > emMeans, unsigned int size, \
							double stay_prob, double step_prob, double skip_prob, double smoothing, vector< vector<unsigned int> > pred, vector< vector<unsigned int> > foll);
// Main
int main(int argc, char* argv[]);

#endif
