#ifndef ALIGNALSDJLAKJFLKQWNFOIPQ
#define ALIGNALSDJLAKJFLKQWNFOIPQ

#include "lib.h"

using namespace std;

// Symbol comaparison (replaces diagonal matrix with 1s on diagonal, -1s otherwise)
int S(char a, char b);
// Gap Penalty
int gap();
// For SW,NW returns max result for insert, delete and substitution with the direction in 2 bits
int max3(int t, int tl, int l, bool &T, bool &L);
// For Edit distance same as max3 only returns minimum
int min3(int t, int tl, int l, bool &T, bool &L);
// Gotoh improvement may follow in previous direction
int maxG(int noJump, int jump, bool &jumped);
//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Needleman-Wunsch Algorithm
int NW(string A, string B, string &alA, string &alB);
//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Smith-Waterman Algorithm (not in C++)
//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Smith-Waterman-Gotoh Algorithm
int SWG(string A, string B, string &alA, string &alB);
//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
// Edit distance
int Edit(string A, string B, string &alA, string &alB);
//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//----------//
//Main
int main(int argc, char* argv[]);

#endif
