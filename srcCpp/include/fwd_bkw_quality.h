#ifndef FORWARDBACKWARDQUALITYASFDLASKJLK
#define FORWARDBACKWARDQUALITYASFDLASKJLK

#include "lib.h"

using namespace std;

// Used for sorting
bool reverseCompare(pair<int, double> i, pair<int, double> j);

// Forward-Backward
string fwdBkw(vector<double> currents, string predictions, vector<int> moves, vector<Table *> transM, vector<pair<double, double> > emMeans, unsigned int size, \
							double stay_prob, double step_prob, double skip_prob, vector< vector<unsigned int> > pred, vector< vector<unsigned int> > foll);
// Main
int main(int argc, char* argv[]);

#endif
