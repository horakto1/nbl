#ifndef VITERBIASDASLDKAASDA
#define VITERBIASDASLDKAASDA

#include "lib.h"

using namespace std;

// Viterbi Algorithm
//----------------------------------------------------------------------------------------------------//
// Viterbi
vector<unsigned int> viterbi(vector<double> currents, vector<Table *> transM, vector<pair<double, double> > emMeans, unsigned int size, \
							double stay_prob, double step_prob, double skip_prob, vector< vector<unsigned int> > pred);
//----------------------------------------------------------------------------------------------------//
// Main
int main(int argc, char* argv[]);

#endif
