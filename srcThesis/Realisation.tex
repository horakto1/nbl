\chapter{Results}\label{chap_Results}
First section \ref{sec_Data} of this chapter presents the dataset, which was used to optimize implemented solutions that are presented in section \ref{chap_Realisation}. Following sections focuses on obtaining the best settings for both Viterbi and Forward--Backward Algorithms. Therefore several runs were executed and graphs and tables showing results are presented. First testing was performed on simulated data in section \ref{sec_resG}, after that on sample set 1 \ref{sec_res1} and lastly on sample set 2 \ref{sec_res2} with the adjusted table of means and standard deviations. Sample set 3 was used only in comparison with other solutions.

Each dataset was tested using $3$, $4$ and $5$-mers. After all test were exhausted the best performing solution was selected \ref{sec_best} and compared with both Nanocall and DeepNano \ref{sec_compare}. The last section \ref{sec_compareC} briefly discusses the time and space complexity of the solution.

\section{Dataset} \label{sec_Data}
Data provided by Nicholas Loman \cite{Data} were used for most of the testing and optimization purposes. These data contain \num[group-separator={,}]{164472} E.coli DNA sequences. The data were recorded during the 28th and 29th of July of 2016. The measuring was performed in six different locations using the same R9 chemistry. Even though the same type of device was used, each of the station will have a different parameters, so the scaling will be necessary.

But first, the dataset as a whole will be presented. The dataset can be summarized by these numbers using base pairs as a unit:

\vspace{10px}
%\centering
\begin{tabular}{ l r }
	Minimal Length:      & \num[group-separator={,}]{177} \\
	Average Length:      & \num[group-separator={,}]{9009} \\
	Maximal Length:      & \num[group-separator={,}]{131\,969} \\
\end{tabular}
\vspace{10px}

The histogram \ref{img_histAll} shows that the lengths have almost geometric distribution if grouped by thousands. Except the very start, where there are not as many sequences shorter than thousand base pairs.

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{img/histogram2}  
\caption{Histogram of sequence lengths in used dataset.}
\label{img_histAll}
\end{figure}

The distribution of individual bases is almost uniform, where both Cytosine and Guanine bases are represented by more than 26 percent, while Adenine is represented a little under 24 percent. This is shown in Figure \ref{img_basesAll}. As such the data are slightly CG rich.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{img/BaseDistr2} 
\caption{Nucleotide distribution in the datatset.}
\label{img_basesAll}
\end{figure}

In subsections below an example sequence will be shown in detail. After that, two sample sets, taken from the introduced dataset, are presented and their properties discussed. Last section shows another sample set, which is on the other hand a measurement of one specific human gene, which should be harder to base-call because of its repetitiveness and different CG to AT ratio. 


\subsection{Example Sequence}
Each sequence is stored in a \textit{fast5} file. Whole tree of the content is shown in Appendix \ref{app_fast5}. One of the sequences will be shown in detail, others have similar properties. The presented sequence is from sample 1 discussed below. It is a~sequence recorded on 29th of July 2016 in the lab with ID of 26075 on channel~115 read 849. It contains \num[group-separator={,}]{5671} bases.

One of the most important parts for this work will be the \textit{Signal} dataset from \textit{Raw} group and the \textit{Events} dataset from \textit{EventDetection} group. The raw data are shown in Figure \ref{fig_RawAll}. As can be seen the data points are mostly between 400 and 800 units. However most of the sequences start and some even end with the measuring highly above the 800 mark. These parts of the sequences will not be taken into account as they are the parts, where the DNA strand was not located inside the nanopore. A closer look in Figure~\ref{fig_RawDetail} shows about 30 base shifts, because the whole dataset consist of \num[group-separator={,}]{96260} valid raw measurements so that leaves almost 17 data points per base move.

\begin{figure}[h]
\centering
\includegraphics[width=0.82\textwidth]{img/Raw_all}
\caption{Example of raw currents in one \textit{fast5} file.}
\label{fig_RawAll}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.82\textwidth]{img/Raw_detail}
\caption{Closer look at 500 data points in one real data file.}
\label{fig_RawDetail}
\end{figure}

The segmentation of raw data creates a sequence of events. In optimal case each event represents one step of the sequence reading, meaning that any single event would be shifted by exactly one base. So, for example if one event represents `TTCAC', the next one can be `TCACG'. However, the segmentation is not always perfect and it is necessary to introduce so called stays and skips. 

Stays are events which made no shift in context from the previous event, in other words, the Hidden Markov Model used a loop back to the same state. This may happen only due to a segmentation error, which wrongly assessed a~shift. Skips, on the other hand, are events with context shifted by more than a single base. Skips may occur due to an error in segmentation as well or due to an irregularity in sequencer, where the strand of DNA moved too fast and the sequencer did not measure enough values for the skipped context. The correct events will be called steps in future references. An example of segmentation using the same sequence is shown in Figure \ref{fig_Group}. Closer look at the segmentation results is in Figure \ref{fig_GroupDetail}. It shows the same part of the raw data in orange, with the segmented section shown in blue. All of those events are stored in the dataset \textit{Events} in the \textit{fast5} file.

\begin{figure}[h]
\centering
\includegraphics[width=0.82\textwidth]{img/Raw_group}
\caption{Example of segmented events in one data file.}
\label{fig_Group}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.82\textwidth]{img/Raw_groupDetail}
\caption{Detail of 50 events in the same data file.}
\label{fig_GroupDetail}
\end{figure}


\subsection{Sample Set 1}
Both selected samples contain exactly hundred sequences. The first of the two sample sets was measured by a station with ID of 26075 on 29th June 2016. \newpage

Main properties are as follows:

\vspace{10px}
%\centering
\begin{tabular}{ l r }
	Minimal Length:      & \num{606} \\
	Average Length:      & \num{4784} \\
	Maximal Length:      & \num{11982}  \\
\end{tabular}
\vspace{10px}

Whereas the full dataset contain sequences over \num{130000} base pairs long, in the sample the longest sequence is only \num{12000} base pairs long. The main reason for choosing shorter sequences is the shorter run time. Closer comparison of the bases and event count is in Figure \ref{img_sample1}, where the ratio between event and base count is visible. There are up to twice as much events which means a high number  of stays will have to be detected. However, it is necessary to take into account some skips as well. By the examination of the provided analysis, the stay probability should be around $0.5$ and skip probability little over $0.002$, although more probabilities will be tested using these as defaults.

\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{img/sample1}
\caption{Event count to Base count ratio for each read in sample 1.}
\label{img_sample1}
\end{figure}

\subsection{Sample Set 2}
The other sample set was measured by a station with ID of 40525 a day earlier on 28th June 2016. The properties are very similar to the previous set and are listed here:

\vspace{10px}
%\centering
\begin{tabular}{ l r }
	Minimal Length:      & \num{528} \\
	Average Length:      & \num{6197} \\
	Maximal Length:      & \num{16208} \\
\end{tabular}
\vspace{10px}

As in previous sample, there are more events recorded than the actual base count (Figure \ref{img_sample2}). Stay and skip probabilities should be similar to the previous sample.

\begin{figure}[h]
\centering
\includegraphics[width=0.95\textwidth]{img/sample2}
\caption{Event count to Base count ratio for each read in sample 2.}
\label{img_sample2}
\end{figure}

\subsection{Sample Set 3}
As was already mentioned, the third sample is a measurement of human gene with a distribution of all nucleotides presented in Figure \ref{img_bases3}. As can be seen, the distribution is very different from the previous dataset. It has lower CG content, although as it describes only single strands without their complements, the number of C and G bases is different.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/s3bases} 
\caption{Nucleotide distribution in sample 3.}
\label{img_bases3}
\end{figure}


On the other hand, some properties are very similar due to the sequencing technology used. The sample contains 100 sequences and the lengths of used strands are as follows:

\vspace{10px}
%\centering
\begin{tabular}{ l r }
	Minimal Length:      & \num[group-separator={,}]{472} \\
	Average Length:      & \num[group-separator={,}]{8043} \\
	Maximal Length:      & \num[group-separator={,}]{11968} \\
\end{tabular}
\vspace{10px}

Ratios between event and base pair counts are also similar to the previous samples, so the skip and stay probabilities should be very similar. The specific values are shown in Figure \ref{img_sample3} and it is again sorted by the shown value.

\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{img/sample3}
\caption{Event count to Base count ratio for each read in sample 3.}
\label{img_sample3}
\end{figure}

\section{Implemented Methods} \label{chap_Realisation}
This section describes the proposed approach to the DNA Base-calling. Introduction to algorithms and the pseudocodes were described in methods \ref{chap_Analysis}, the focus of this chapter is on the practical implementation and usage of both Viterbi and Foward--Backward algorithms as well as the supportive scripts, which were used for processing the data and the outputs. In the first two sections \ref{sec_Vimpl} and \ref{sec_FBimpl} both HMM algorithms are discussed. Next section \ref{sec_DataGen} will show data simulation for the initial test and the last section \ref{sec_alignImpl} will discuss mainly the implementation of the alignment algorithm.

It might be helpful to first introduce a flexible toolkit \textit{Poretools} \cite{Poretools}, which was created by Loman and Quinlan in 2014 in order to easily handle \textit{fast5} files generated by the MinION. Currently the device generates single \textit{fast5} file for each read, which results in a large number of files. Thus the \textit{Poretools} can be used on whole directories or single files. It is written in Python and is easy to use. It is possible to convert \textit{fast5} files to either \textit{fastq} files or to \textit{fasta} files which are expected as an input in the proposed application.

However, considering that the data were filtered only once and it was more convenient to do so using a script, instead a \textit{h5py} \cite{Pythonh5py} library was used to not only extract both events and reference sequences from the \textit{fast5} files, but also to analyse all features of the dataset. This is the source of the information given in section \ref{sec_Data}.

\subsection{Viterbi Algorithm}\label{sec_Vimpl}
Viterbi algorithm was chosen due to its proven usefulness in this area. Timp \textit{et al.} in 2012 \cite{NanoporeViterbi_Winston} proposed using Viterbi algorithm as a high accuracy base-caller. It was not tested on real sequences, but on simulated data, which should be generated using the same parameters the actual data are distributed. However, one very important and not very accurate assumption was made. The generated data used optimally segmented events, so the algorithm did not have to consider stays and skips and instead could take into account only steps by exactly one base. The algorithm performs well despite the simplification, which was later confirmed by others like \cite{Nanocall}. The algorithm just needs to take into account stays and skips by adjusting transition probabilities.

In this approach only the scaling of parameters and base-calling steps are implemented as the segmentation is not only a rather difficult process to perform accurately, but the given dataset provided already segmented data as well. Also the Metrichor platform segments the data locally so it is not necessary to create a new segmentation tool. On the other hand, both parts implemented are usable for computation with $3$-mers, $4$-mers and $5$-mers as contexts. Those three versions were tested, however other sizes should be viable without further modifying the solution.

\subsubsection{Scaling of Parameters}
The default parameters for the algorithm were based on means and standard deviations taken from the sample 1 presented in section \ref{sec_Data}. The dataset Events contains means, standard deviations and matching $5$-mers, so using this information it is possible to reconstruct every value for up to $5$-mers. When reconstructing means and standard deviations for currents for smaller $k$-mers, the last nucleotides were taken into account as they should be those which entered the pore last and thus influenced the ion current values.

These means and standard deviations were used in testing on sample set 1. However for the sample set 2, those values would not be optimal. Therefore, a simple adjustment using a normalization similar to Min-Max is used. The assumption is that both compared datasets will have the same distribution of currents. Then the $k$-mer with lowest and the highest mean current will have the value at the similar percentile of the data. Thus it is possible to get new values for those two $k$-mers and use their values as new Min and Max. After that the new value of mean current for each $k$-mer is computed like this:

$$\mu _{new} = (max_{new} - min_{new}) \dfrac{\mu _{old} - min_{old}}{max_{old} - min_{old}}  + min_{new}$$

Measured percentiles and matching $k$-mers are shown in Table \ref{tab_minMaxScaling}. This is a relatively simple solution and it should work for sample 2 mostly because both datasets are measured using E.coli DNA strands. When this scaling is used on DNA from different organism it may yield skewed results, because the ratio of CG to AT nucleotides may not be the same and thus the distributions of currents will differ as well. 

\begin{table}[h]
\centering
\begin{tabular}{ | c | r | r || r | r | }
\hline
 & \multicolumn{2}{c ||}{Min} & \multicolumn{2}{c |}{Max} \\ \hline
 $k$ & \multicolumn{1}{c|}{$k$-mer} & \multicolumn{1}{c||}{perc.} & \multicolumn{1}{c|}{$k$-mer} & \multicolumn{1}{c|}{perc.} \\ \hline
 3 & GTT & 2.7179 & TCA & 96.9545 \\ \hline
 4 & GGAT & 0.5402 & ATCA & 98.9582 \\ \hline
 5 & GGGAT & 0.3041 & GATCC & 99.2965 \\ \hline
\end{tabular}
\caption{Percentiles of contexts with minimal and maximal current in sample~1.}
\label{tab_minMaxScaling}
\end{table}

To confirm the assumption about the distribution of currents a histogram \ref{fig_currentsDistr} is presented. It is visible that the values are slightly shifted and are wider in sample 2 compared to sample 1, otherwise they both have two similar peaks. The decrease in the middle is caused by steeper increase in currents between corresponding contexts in the middle of the table.

\begin{figure}[h]
\centering
\includegraphics[width=1\linewidth]{img/currentsRepr2}
\caption{Comparison of current representations in used data samples.}
\label{fig_currentsDistr}
\end{figure}

Also, as was predicted, sample 3 follows a different curve, so the scaling method may not be as viable as for the sample 2. The last curve represents the distribution of currents in the whole E.coli dataset. As the dataset contains measurements from several laboratories with differently shifted values, the decrease in the middle of the curve is smoothed out and almost perfect Gaussian curve is created. As can be seen, sample 1 is shifted more from the average sequence, although it can still be considered common.

The scaling was done by a Python 2.7 script \textit{adjustTable.py}, which requires the size of $k$-mers used, the default table of means and standard deviations of currents for each context, \textit{fasta} file of the sequences to be base-called and the name of output table.

\nocite{Python} 

\subsubsection{Base-calling}
The process of base-calling is then implemented both in Python 2.7 (file \textit{viterbi.py}) and in C++11 (file \textit{viterbi.cpp}). The C++ version can be compiled by attached \textit{Makefile}. All algorithms also implemented in C++ are included in the \textit{Makefile} as well.

The run of base-calling using Viterbi Algorithm requires an input file with segmented currents, a table with means and standard deviations of currents, name of the output file, size of $k$-mers, stay probability and skip probability. The results will be measured using the size of $k$-mers from 3 to 5 and will iterate over both stay and skip probability with the default values set to the one determined in the section \ref{sec_Data}.

The procedure of Viterbi algorithm was previously discussed, here we show the probability computations. The probabilities of the first $k$-mer was uniformly distributed as no part of the sequence is known. 

After that the transition probabilities were computed this way:

$$P_{(A\rightarrow B)} = p_{stay} * \delta_{A=B} + p_{step} * \delta_{\text{follows}(A,B,1)} + p_{skip} * \delta_{\text{follows}(A,B,2)},$$

where $p_{step}$ is the complement of $p_{stay} + p_{skip}$ to 1 and the \text{follows}$(A,B,i)$ function returns whether $k$-mer $B$ is reachable from $k$-mer $A$ using exactly $i$~steps. This is an approximation in terms of not taking into account skips with the length of three and more. The adjustment was made mostly due to the acceleration of the computation by a significant amount. The simplification is possible because it should not affect accuracy much. The $p_{skip}$ value is already really small and thus the longer skips would have even lower probability, which means that long skips are extremely rare and as such would be very hard to predict even if the algorithm expected skips of arbitrary length.

Other parts of the algorithm were not modified, so after finishing the core computing, backtrack is performed and then the base sequence is reconstructed using the smallest number of base shifts possible between the neighbouring predicted contexts.

\subsection{Forward--Backward Algorithm} \label{sec_FBimpl}
Forward--Backward algorithm was implemented twice, each with a different purpose. First uses this algorithm for its intended purpose. It uses an output from the base-caller and assesses the quality of each base using Phred quality score. The score is computed using this formula:

$$Q = -10 \log_{10} P,$$

where P is probability of an error. The probability is computed using an average value for all k-mers, which contain specific base. An output of this algorithm is the default \textit{fastq} file, where the quality is in Phred+33 format. This means it is representable in ASCII characters from `!' for the lowest probability to `I' for the highest probability.

The implementation is in \textit{fwd\_bkw\_quality.cpp} script and is also included in the \textit{Makefile}. The other use of the Forward--Backward algorithm is more experimental and thus will be discussed more thoroughly in the next section.

\subsubsection{Experimental Approach}
The Forward--Backward algorithm is not suitable for base-calling by itself due to the fact that its purpose is only to compute the hidden state with highest probability for each event. One could consolidate $k$-mers into a base sequence, but attempting to do this mostly results in a much longer sequence than expected, because the transitions are not taken into account during the Consolidation phase. It is not uncommon to see four or five bases long skips.

However, the issue can be addressed by modifying the Consolidation phase. The idea is to go through all events and find the state with the highest probability as usual, however if a skip of length three or more is encountered, the algorithm looks at other probabilities in descending order and if it finds a $k$-mer which follows the previous $k$-mer with shorter skip or even as a stay/step, this $k$-mer is used instead. However, it stops going through other states if the probability is too low. The boundary is set by a new input parameter called \textit{smoothing}, which is used as a multiplier.

Another improvement is expected if the consolidation which ignores some events is used. The idea behind it is that many times the consolidation phase encounters two long skips immediately after each other. This may be caused by the nature of Forward--Backward algorithm, where the sequence of predicted $k$-mers is for example `TCAAA', `GGGGG' and `AAATG'. There the third $k$-mer follows the first one using two steps (consolidated as `TCAAATG'), but the second $k$-mer does not fit between them, which may be caused by some sequencing anomaly. The modified consolidation removes the 'GGGGG' event, because it neither follows the previous event, nor is followed by the next event.

Both of these experimental improvements are optional using input parameters and as such will be tested if they show any potential to be of any use. Again this algorithm is implemented in both Python and C++.

\subsection{Simulated Data Generation}\label{sec_DataGen}
The algorithm was optimized and tested using simulated data. For this purpose there is a Python script \textit{generateGrouped.py}. All random sampling was performed using \textit{Random} library provided by Python distribution \cite{PythonRandom}. The input parameters of the script are as follows:

$$minLength\quad maxLength\quad count\quad size\quad stay\_prob\quad skip\_prob\quad table.csv$$

$Min$ and $Max\ lengths$ are the base count limitations for each sequence, $count$ is the number of sequences to generate, $size$ is the size of $k$-mers for generating data points, $stay$ and $skip\ probabilities$ are the probabilities discussed above and $table$ contains the means and standard deviations of data points for each $k$-mer.

The actual simulation is relatively simple and uses several simplifications. First, bases are sampled from uniform distribution, so the CG to AT ratio should be around $0.5$. Currents are then generated using the $k$-mers and their mean and standard deviation of data points. The stay and skip probabilities are taken into account as well. The table obtained from sample 1 was used for the final simulated dataset, so the data should be relatively similar to the segmented real data.

\begin{figure}[h]
\centering
\includegraphics[width=0.99\linewidth]{img/genAll}
\caption{All Events of Generated Sequence.}
\label{fig_genAll}
\end{figure}

A sample of 20 sequences with the length between \num{5000} and \num{15000} base pairs was simulated. Stay probability was set to 0.5 and skip to 0.002 as the real sample set 1 suggested. An example of one sequence is shown in Figure~\ref{fig_genAll} and the detail of 50 events is shown in Figure~\ref{fig_gen50}.

\begin{figure}[h]
\centering
\includegraphics[width=0.99\linewidth]{img/gen50}
\caption{50 Events of Generated Sequence.}
\label{fig_gen50}
\end{figure}

\subsection{Alignment Implementations} \label{sec_alignImpl}
Last required step that needs to be implemented is the alignment algorithm, which was used to measure accuracy of base-calling using the predicted and reference sequences. For this purpose, three alignment techniques presented in Methods were implemented. Smith--Waterman algorithm as local alignment \ref{SW}, Needleman--Wunsch algorithm as global alignment \ref{NW} and modified edit distance \ref{Edit_Dist}. As these methods have similar structure and purpose, they are all in one script called \textit{align.cpp} (\textit{align.py} for Python).

The absolute values returned by the alignment algorithms depend on the sequence length, therefore a normalized value is necessary for better comparison of results. Thus, the alignment value for local and global methods is computed like this:
 
$$result = \dfrac{2 value}{Reference.length + Predicted.length}$$

and for edit distance:

$$result = \dfrac{Reference.length + Predicted.length - 2 value}{Reference.length + Predicted.length}$$

The difference in formulas is due to the fact, that local and global alignment returns higher values for better alignment. On the other hand edit distances uses a sum of insertions, deletions and substitutions, so the lower the value the better the result. Using these formulas, predictions of different sizes are penalized and the maximum value is 1.

After that a simple script called \textit{checkAcc.py} goes through the resulting alignments and finds minimal, maximal, mean and standard deviation values for each alignment. This will be used in presenting results in the following sections.

\input{genResults}

\input{s1Results}

\input{s2Results}


\section{Method Selection} \label{sec_best}
The best performing solution is the Viterbi algorithm using context size 5. For used datasets the stay probability of 0.4 and skip probability of 0.001 performs best if the edit distance alignment is taken as the decisive element. Forward--Backward algorithm proved to be too inaccurate even with modifications in the Consolidation phase. Still, both modifications increased the accuracy by a significant amount. 

Using smaller context size also reduces the accuracy. With $4$-mers the method can achieve decent results and if the computation time is a consideration, it may be a viable option. Using $3$-mers is not advised at all, the decrease in accuracy is too big.

In the paragraphs below, this method will be referred to as Nanopore Base-caller Lite (NBL). In all future references, the default settings use contexts of 5 bases, stay probability of 0.4 and skip probability of 0.001.

\section{Comparison with Existing Methods} \label{sec_compare}
This section will compare NBL, the method defined in previous section, with both Nanocall and DeepNano solutions. These two solutions were selected as they are both open source, thus easily obtainable, and claim to provide accurate results comparable to Metrichor. Nanocall method is similar to NBL, so the comparison is at hand. DeepNano on the other hand uses different approach so it serves as a complement to possible solutions. The comparison will be made on both sample set 1 and sample set 2 using edit distance alignment with the reference sequences.

On those samples, the evaluation was based on similarity with state-of-the-art base-caller. Instead, in sample 3 a BWA-MEM alignment of the base-called sequence to the reference (human) genome is performed, which allows to count the number of mismatches, insertions and deletions for all base-callers. Except for natural polymorphism, it should be possible to measure the real error rate of the base-calling.

Sample 1 and 2 came from two distinct experiments, but the sequences in sample 3 are a mixture of multiple experiments performed at different time, place and protocol chosen specifically by their alignment score using BWA-MEM algorithm.

\subsection{Nanocall Overview}
Nanocall is based on Viterbi algorithm as well, however it uses $6$-mers as context lengths. It may provide the crucial advantage for Nanocall as the results has shown, that the longer the context the better the results. However, the dataset analysis used only $5$-mers as a context so it may not prove as a~significant advantage.

\subsection{DeepNano Overview}
DeepNano solution is based on Recurrent Neural Network. Its main advertised advantage is that it may capture dependencies longer than the length of a context. It is certainly true that in the DNA there are those kind of dependencies, however the impact on base-calling is hard to predict.

\subsection{Burrows--Wheeler Aligner}
Burrows--Wheeler Aligner (BWA) \cite{BWA} is a software package for mapping base-called sequences against a large reference genome, such as the human genome. It consists of three algorithms, but only the MEM algorithm is recommended for this type of project. It works by seeding alignments with maximal exact matches (MEMs) and then extending seeds with the affine-gap Smith--Waterman algorithm. 

BWA is widely used for the result assessment. Both Nanocall and DeepNano used it, so it should provide unbiased comparison between all used implementations.
 
\subsection{Sample 1 Comparison}
On sample 1, NBL has a significant advantage as it was specifically optimized for this dataset. Both Nanocall and DeepNano were used with their default settings, except for specifying R9 chemistry to DeepNano. Comparison of all three solutions is shown in Figure \ref{fig_s1Comp}. The results are shown using violin plot, with mean value in the middle. As in previous figures, the alignments shown are performed by edit distance.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{img/s1_vi}
\caption{Sample 1, Comparison of Results}
\label{fig_s1Comp}
\end{figure}


%\begin{figure}[h]
%\begin{minipage}{.33\linewidth}
%\centering
%Minimum
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%Mean
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%Maximum
%\end{minipage}\par\medskip
%
%\begin{minipage}{.33\linewidth}
%\centering
%\subfloat{\label{s1:g}\includegraphics[width=\textwidth]{img/compS1/el}}
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%\subfloat{\label{s1:h}\includegraphics[width=\textwidth]{img/compS1/em}}
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%\subfloat{\label{s1:i}\includegraphics[width=\textwidth]{img/compS1/eh}}
%\end{minipage}\par\medskip
%
%\caption{Sample 1, Comparison of Results}
%\label{fig_s1Comp}
%\end{figure}

The results on a 100 randomly selected sequences show, that the implemented version of Viterbi algorithm performs better than the Nanocall solution, although Nanocall is disadvantaged by the need to adjust all settings by itself. DeepNano and its Recurrent Neural Network displays significantly better results, which indicates its advantage over both HMM based methods.

Overall, the RNN proved to be more accurate than HMM models in agreement with \cite{DeepNano}. Nanocall has shown lower accuracy in comparison to NBL, however the cause for this is probably the need to use scaled parameters, which is both hard to perform precisely and very important for the resulting accuracy.

\subsection{Sample 2 Comparison} \label{sec_comp2}
On sample 2, the advantage of knowing the settings for implemented solution is not so prominent, because the scaling had to be used even in the NBL. Nevertheless, the dataset still contains similar E.coli sequences and was measured by the same chemistry.

Results are shown in the same manner as in previous section in Figure \ref{fig_s2Comp}. Nanocall is again indicated by green color, DeepNano is orange and NBL is blue.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{img/s2_vi}
\caption{Sample 2, Comparison of Results}
\label{fig_s2Comp}
\end{figure}

%\begin{figure}[h]
%\begin{minipage}{.33\linewidth}
%\centering
%Minimum
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%Mean
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%Maximum
%\end{minipage}\par\medskip

%\begin{minipage}{.33\linewidth}
%\centering
%\subfloat{\label{s2:g}\includegraphics[width=\textwidth]{img/compS2/el}}
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%\subfloat{\label{s2:h}\includegraphics[width=\textwidth]{img/compS2/em}}
%\end{minipage}%
%\begin{minipage}{.33\linewidth}
%\centering
%\subfloat{\label{s2:i}\includegraphics[width=\textwidth]{img/compS2/eh}}
%\end{minipage}\par\medskip
%
%\caption{Sample 2, Comparison of Results}
%\label{fig_s2Comp}
%\end{figure}

The results show similar performance for DeepNano with overall performance significantly better than both HMM solutions. Nanocall reports lower maximal accuracy than NBL, but in average case reaches higher values. The reason for the decrease in accuracy of NBL is probably due to incorrect use of scaling, where it should be applied for each read separately.

To sum both comparisons, the implemented solution performed well to certain extent, although it was designated exactly for the given dataset. DeepNano solution achieved the best results with the lowest variance. Nanocall is not as accurate, but on the other hand shows stable results on both samples. The numeric values of DeepNano and Nanocall results are shown in Appendix~\ref{app_DNNC}.

\subsection{Sample 3 Comparison}
Last comparison is performed on sample 3. It does not use the edit distance alignment as was already mentioned. The comparison is made using BWA-MEM algorithm against the real human gene, which was base-called. To avoid the problem with scaling present in sample 2, which may be magnified in this sample because of its heterogeneity, all input parameters were adjusted for each read separately. Figure \ref{fig_s3comp} shows error rate for each of the 100 sequences base-called. The error rate is computed from CIGAR string provided in the mapping result.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{img/bwa_error2}  
\caption{Error rate comparison on sample 3 using BWA-MEM.}
\label{fig_s3comp}
\end{figure}

The CIGAR string is a sequence of base lengths and the associated operation. The main operations are hard clip (H), soft clip (S), insertion (I), deletion (D) and match (M). Clips can occur only at the start and the end of the sequence and signalize, that the aligned sequence had to be shortened to be successfully mapped, thus may be considered as insertions at the ends. An~example CIGAR string looks like this:

\begin{equation}
  \texttt{10S30M5I1D20M2S} \\
\end{equation}

It shows a very short alignment with 10 bases clipped at the start and 2 at the end. 50 matches in total and 5 insertions and 1 deletion. The resulting error rate would be $\frac{10+2+5+1}{30+20} = \frac{18}{50} = 0.36$. Therefore, the error rate is an average number of errors per match.

From the figure, it is visible that Metrichor \ref{Metrichor} achieves the best result on most sequences. There are several possible reasons. First is that the sequences were selected based on the achieved result by Metrichor, so other sequences could provide more balanced results. Also, Metrichor could utilize some information from complement strand whose measurement is sometimes concatenated to the template strand in the \textit{fast5} file.

Otherwise, if only the three selected solutions are compared, DeepNano solution performs best on most reads. Average values per read are presented in Table \ref{tab_s3}. DeepNano achieves significantly lower error rate than both HMM based solutions. On the other hand, Nanocall creates longer mapped sequences with more than 100 bases difference in matches. The NBL solution does not outperform other solutions in any category, but the results are competitive.

\begin{table}[h]
\centering
\begin{tabular}{ | l | r | r | r | }
\cline{2-4}
 \multicolumn{1}{ c |}{} & \multicolumn{1}{ c |}{\cellcolor{Gray} NBL}  & \multicolumn{1}{ c |}{\cellcolor{Gray} Nanocall} & \multicolumn{1}{ c |}{\cellcolor{Gray} DeepNano} \\ \hline
 Clips & 155 & 144 & 116 \\ \hline
 Insertions/Deletions & 788 & 827 & 732 \\ \hline
 Matches & 4712 & 4833 & 4711 \\ \hline
 Error Rate & 0.205 & 0.209 & 0.183 \\ \hline
\end{tabular}
\caption{Average BWA-MEM alignment result comparison on sample 3.}
\label{tab_s3}
\end{table}

The last read shows very high error rate even using Metrichor. All reads were selected using only clip, insertion and deletion count and this read was only several hundred base pairs long, therefore the error rate was affected. In this case both Nanocall and NBL generated a sequence which was not mapped at all. The figure shows value 1 so the other values were still recognizable. All numeric values of error rate are shown in Appendix \ref{app_s3ER}.

\section{Time and Space Complexity} \label{sec_compareC}
The last comparison between the three solutions is not about performance in base-calling, instead it focuses more on the hardware requirements. The first attribute is time complexity. The main disadvantage of NBL solution is the lack of parallelization. DeepNano uses four threads in the default settings, which is not adjustable by an input parameter. Nanocall can be run using arbitrary number of threads. The disadvantage can be partly evaded by running several instances of NBL, because the base-calling  of one sequence does not affect other sequences, although the parallelization requires extra effort. The time complexity of NBL for one read can be interpreted as:

$$T_{NBL} = \mathcal{O}(N_e \times 4^{k} \times tr),$$

where $N_e$ is number of events, $k$ is the size of context and $tr$ is the number of possible transitions from each state. As was already discussed, the value of $tr$ is up to 21 in current implementation.

Table \ref{tab_timeComp} shows performance using events per second (eps) which was achieved on the whole sample 1. All times are measured for the run of the whole process as it was used as a black box. Every run was measured on processor Intel Core i7-4700MQ CPU @ $2.40\,\mathrm{GHz} \times 8$.

\begin{table}[h]
\centering
\begin{tabular}{ | l | c | c | r | }
\hline
\rowcolor{Gray}
Tool & Threads & Context & Eps \\ \hline
NBL & 1 & 3-mers & \num{20287} \\ \hline
NBL & 1 & 4-mers & \num{4333} \\ \hline
NBL & 1 & 5-mers & \num{975} \\ \hline
Nanocall & 1 & 6-mers & \num{565} \\ \hline
Nanocall & 4 & 6-mers & \num{1513} \\ \hline
DeepNano & 1 & N/A & \num{836} \\ \hline
DeepNano & 4 & N/A & \num{2824} \\ \hline

\end{tabular}
\caption{Events per second comparison in base-calling of sample 1.}
\label{tab_timeComp}
\end{table}

From the table, it is visible that if run on single core without multi-threading, the NBL is the fastest. However, the parallelization speeds the computation by a significant amount. Also as expected, if smaller contexts are used, the number of events per second processed is significantly higher at the cost of lower accuracy.

Another attribute is space complexity. Table \ref{tab_spaceComp} shows the peak  memory usage of each tool during the computation of sample 1 base-calling.

\begin{table}[h]
\centering
\begin{tabular}{ | l | c | r | }
\hline
\rowcolor{Gray}
Tool & Context & Kilobytes \\ \hline
NBL & 3-mers & \num{18992} \\ \hline
NBL & 4-mers & \num{61980} \\ \hline
NBL & 5-mers & \num{250792} \\ \hline
Nanocall & 6-mers & \num{580260} \\ \hline
DeepNano & N/A & \num{93872} \\ \hline
\end{tabular}
\caption{Peak memory usage of base-callers on sample 1 in kilobytes.}
\label{tab_spaceComp}
\end{table}

Again as expected, the values strongly depend on the size of context in NBL solution. And as the DeepNano does not use HMM and dynamic programming, it requires smaller amount of memory unless using very short context size. The memory requirement for NBL can be expressed as:

$$M_{NBL} = \mathcal{O}(N_e \times 4^{k}),$$

using the same variables as in time complexity. The value $4^k$ in these formulas is the number of contexts and therefore the number of states of the Hidden Markov Model. 

In summary, DeepNano excels in both presented comparisons, unless using very short contexts in NBL. However, the difference in accuracy for shorter contexts is so significant, it is not worth any consideration in most cases. Based only on these parameters, the RNN seems to have only advantages.

